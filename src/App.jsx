import React, { useState, useEffect, useCallback } from "react";

import useStore from "./store";
import { apiStatuses } from "./constants";
import { quaternionToEulerAngles } from "./utils";

import Modal from "./components/Modal";

import elderWand from "./images/elder-wand.png";
import loader from "./images/nice-hp-oop.gif";
import "./styles.css";

// eslint-disable-next-line no-undef
const sensor = new AbsoluteOrientationSensor({ frequency: 60 });

export default function App() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { appendAttitude, startTracking, predict, prediction } = useStore();

  const predicting = prediction.status === apiStatuses.fetching;

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const handleStartTracking = () => {
    startTracking(sensor);
  };

  const handleStopTracking = () => {
    predict(sensor);
  };

  const handleSensor = useCallback(
    (e) => {
      const { quaternion } = e.target;
      const angles = quaternionToEulerAngles(quaternion);
      appendAttitude([angles.heading, angles.bank]);
    },
    [appendAttitude]
  );

  useEffect(() => {
    sensor.addEventListener("reading", handleSensor);

    return () => {
      sensor.removeEventListener("reading", handleSensor);
    };
  }, [handleSensor]);

  return (
    <div className="container">
      <div className="header">
        <h2>The Elder Wand</h2>
        <i className="gg-info" onClick={openModal}></i>
      </div>

      <div className="predContainer">
        {prediction.data && (
          <div className="prediction">
            <h4>Prediction: {prediction.data.prediction}</h4>

            <div className="spell">
              <h4>Your Spell:</h4>
              <img
                src={`data:image/png;base64,${prediction.data.img}`}
                alt="predicted digit"
              />
            </div>
          </div>
        )}

        {predicting && <img alt="loader" src={loader} className="loader" />}

        {prediction.error && (
          <h4 className="errorMsg">Error: {prediction.error}</h4>
        )}
      </div>

      <img
        disabled={predicting}
        onTouchStart={handleStartTracking}
        onTouchEnd={handleStopTracking}
        onContextMenu={(e) => e.preventDefault()}
        src={elderWand}
        alt="elder-wand"
        className="wand"
      />

      <h5>
        Made with 🪄 by <a href="https://twitter.com/27upon2">Bharath</a>
      </h5>

      <Modal
        title="How to use this?"
        show={isModalOpen}
        handleClose={closeModal}
      >
        <p>
          Press the wand and draw your spell in air with your phone or tablet so
          that the screen faces upwards. Stop pressing the wand when you are
          done and you will see the prediction and the spell you drew.
        </p>
      </Modal>
    </div>
  );
}
