import "./styles.css";

const Modal = ({ handleClose, show, title, children }) => {
  const className = show ? "modal display-block" : "modal display-none";

  return (
    <div className={className}>
      <section className="modal-main">
        <header>
          <h2>{title}</h2>
          <i className="gg-close" onClick={handleClose}></i>
        </header>

        {children}
      </section>
    </div>
  );
};

export default Modal;
