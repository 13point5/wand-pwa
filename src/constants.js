const API_URL = "http://localhost:5000";

export const apiEndpoints = {
  predictDigit: `${API_URL}/predictSpell`,
};

export const apiStatuses = {
  idle: 0,
  fetching: 1,
  complete: 2,
};
