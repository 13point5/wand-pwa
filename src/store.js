import axios from "axios";
import createStore from "zustand";

import { apiEndpoints, apiStatuses } from "./constants";

const predictionInitialState = {
  status: apiStatuses.idle,
  data: null,
  error: null,
};

const useStore = createStore((set, get) => ({
  attitudes: [],
  appendAttitude: (angle) =>
    set((state) => ({
      attitudes: [...state.attitudes, angle],
    })),

  prediction: predictionInitialState,
  startTracking: (sensor) => {
    // reset the prediction state
    set(() => ({
      prediction: predictionInitialState,
    }));

    // start tracking sensor data
    sensor.start();
  },
  predict: async (sensor) => {
    // stop tracking sensor data
    sensor.stop();

    // reset the prediction state
    set(() => ({
      prediction: { ...predictionInitialState, status: apiStatuses.fetching },
    }));

    // Hit the API
    try {
      const { attitudes } = get();

      const { data } = await axios.post(apiEndpoints.predictDigit, {
        attitudes,
      });

      set(() => ({
        prediction: {
          status: apiStatuses.complete,
          data,
          error: null,
        },
      }));
    } catch (error) {
      set(() => ({
        prediction: {
          status: apiStatuses.complete,
          data: null,
          error: error.message,
        },
      }));
    } finally {
      set(() => ({
        attitudes: [],
      }));
    }
  },
}));

export default useStore;
