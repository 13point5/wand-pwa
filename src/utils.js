// Source: https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles

// Heading – psi : rotation about the Z-axis
// Pitch – theta : rotation about the new Y-axis
// Bank – phi : rotation about the new X-axis

export const quaternionToEulerAngles = (q) => {
  const [q0, q1, q2, q3] = q;

  const psi = Math.atan2(2 * (q0 * q1 + q2 * q3), 1 - 2 * (q1 * q1 + q2 * q2));

  const theta = Math.asin(2 * q0 * q2 - q3 * q1);

  const phi = Math.atan2(2 * (q0 * q3 + q1 * q2), 1 - 2 * (q2 * q2 + q3 * q3));

  return { heading: psi, pitch: theta, bank: phi };
};
